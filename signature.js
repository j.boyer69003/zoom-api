//include required modules
const jwt = require('jsonwebtoken');
const config = require('./config');
const rp = require('request-promise');
const axios = require('axios');
var cors = require( 'cors' )
const crypto = require('crypto') // crypto comes with Node.js
const KJUR = require('jsrsasign');
const express = require( 'express' );
const bodyParser = require('body-parser')
const app = express();
var email, userid, resp;
const port = 4000;

app.use(express.urlencoded({ extended: true }));
app.use(express.static('public'));
app.use( bodyParser.json(), cors() )
app.use(cors())
require('dotenv').config()

const payload = {
    iss: config.APIKey,
    exp: ((new Date()).getTime() + 5000)
};
const token = jwt.sign(payload, config.APISecret);


//get the form 
app.get( '/', ( req, res ) =>
{
    return res.json( true )
});


// POST: meeting: 
/**
    "meetingNumber": 123456789,
    "role": 0
*/
app.post( '/signature', ( req, res ) =>
{
  // ROLE  `1` for meeting hosts; `0` for participants & joining webinars
  console.log(req.body.meetingNumber)
  const timestamp = new Date().getTime() - 30000
  const msg = Buffer.from(process.env.API_KEY + req.body.meetingNumber + timestamp + req.body.role).toString('base64')
  const hash = crypto.createHmac('sha256', process.env.API_SECRET).update(msg).digest('base64')
  const signature = Buffer.from(`${process.env.API_KEY}.${req.body.meetingNumber}.${timestamp}.${req.body.role}.${hash}`).toString('base64')

  return res.json({
    signature: signature
  } )
});

//use userinfo from the form and make a post request to /userinfo
app.post('/userinfo', (req, res) => {
    email = req.body.email;
  const options = {
    //You can use a different uri if you're making an API call to a different Zoom endpoint.
    uri: "https://api.zoom.us/v2/users/"+email, 
    qs: {
        status: 'active' 
    },
    auth: {
        'bearer': token
    },
    headers: {
        'User-Agent': 'Zoom-api-Jwt-Request',
        'content-type': 'application/json'
    },
    json: true //Parse the JSON string in the response
};

//Use request-promise module's .then() method to make request calls.
rp(options)
    .then( async (response)=> {
      //printing the response on the console
        console.log('User has', response);
        //console.log(typeof response);
        resp = response
        
        const url = await axios.post( `https://api.zoom.us/v2/users/${ response.id }/meetings`, {
            firstName: 'DJ',
            lastName: 'Confinement'
        }, {
            headers: {
                Authorization: "Bearer " + token,
                'content-type': 'application/json'
            },
        });
        console.log(url)
        
        //Adding html to the page
        var title1 ='<center><h3>Your token: </h3></center>' 
        var result1 = title1 + '<code><pre style="background-color:#aef8f9;">' + token + '</pre></code>';
        var title ='<center><h3>User\'s information:</h3></center>' 
        //Prettify the JSON format using pre tag and JSON.stringify
        var result = title + '<code><pre style="background-color:#aef8f9;">'+JSON.stringify(resp, null, 2)+ '</pre></code>'
        res.send(result1 + '<br>' + result);
 
    })
    .catch(function (err) {
        // API call failed...
        console.log('API call failed, reason ', err);
    });


});


app.listen(port, () => console.log(`Example app listening on port ${port}!`));